package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/xanzy/go-gitlab"
)

const (
	gitlabDocsRepo string = "gitlab-com/gitlab-docs"
)

// Truncate the remote docs branch name if it's more than 63 characters
// otherwise we hit the filesystem limit and the directory name where
// NGINX serves the site won't match the branch name.
func docsBranch() string {
	// The maximum string length a file can have on a filesystem (ext4)
	// is 63 characters. Let's use something smaller to be 100% sure.
	max := 42
	// Prefix the remote branch with 'preview-' in order to avoid
	// name conflicts in the rare case the branch name already
	// exists in the docs repo and truncate to max length.
	branch := "preview-" + os.Getenv("CI_COMMIT_REF_SLUG")
	if len(branch) > max {
		branch = branch[:max]
	}

	return branch
}

func deploy(branch, component, docProject string, client *gitlab.Client) error {
	opts := &gitlab.CreateBranchOptions{
		Branch: gitlab.String(branch),
		Ref:    gitlab.String("master"),
	}

	branchExisted := false
	_, _, err := client.Branches.CreateBranch(docProject, opts)
	if err != nil {
		if errorResp, ok := err.(*gitlab.ErrorResponse); ok {
			if errorResp.Response.StatusCode == http.StatusBadRequest {
				branchExisted = true
			}
		}

		if !branchExisted {
			return err
		}
	}

	if branchExisted {
		fmt.Println("Remote branch", branch, "already exists")
	} else {
		fmt.Println("Remote branch", branch, "created")
	}

	pipelineOpts := &gitlab.RunPipelineTriggerOptions{
		Token: gitlab.String(os.Getenv("CI_JOB_TOKEN")),
		Ref:   gitlab.String(branch),
		Variables: map[string]string{
			component: os.Getenv("CI_COMMIT_REF_NAME"),
		},
	}
	pipeline, _, err := client.PipelineTriggers.RunPipelineTrigger(docProject, pipelineOpts)
	if err != nil {
		return err
	}

	componentSlug := component[len("BRANCH_"):]
	reviewAppURL := fmt.Sprintf("http://%s.%s/%s", branch, os.Getenv("DOCS_REVIEW_APPS_DOMAIN"), strings.ToLower(componentSlug))

	fmt.Println("=> Pipeline created:")
	fmt.Println()
	fmt.Printf("https://gitlab.com/gitlab-com/gitlab-docs/pipelines/%v\n", pipeline.ID)
	fmt.Println()
	fmt.Println("=> Preview your changes live at:")
	fmt.Println()
	fmt.Println(reviewAppURL)
	fmt.Println()
	return nil
}

func printAndExit(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("usage: %s deploy|cleanup\n", os.Args[0])
		os.Exit(1)
	}

	var err error
	branch := docsBranch()
	client := gitlab.NewClient(nil, os.Getenv("DOCS_API_TOKEN"))
	target := os.Getenv("TARGET_PROJECT")
	if target == "" {
		target = gitlabDocsRepo
	}

	switch os.Args[1] {
	case "deploy":
		var component string
		switch os.Getenv("CI_PROJECT_NAME") {
		case "gitlab-ce":
			component = "BRANCH_CE"
		case "gitlab-ee":
			component = "BRANCH_EE"
		case "gitlab-runner":
			component = "BRANCH_RUNNER"
		case "omnibus-gitlab":
			component = "BRANCH_OMNIBUS"
		default:
			fmt.Println("Unknown component")
			os.Exit(2)
		}

		err = deploy(branch, component, target, client)
		if err != nil {
			printAndExit(err)
		}

	case "cleanup":
		fmt.Println("Deleting remote branch", branch)
		_, err = client.Branches.DeleteBranch(target, branch)
		if err != nil {
			printAndExit(err)
		}
	default:
		err = fmt.Errorf("unknonw command")
	}

}
