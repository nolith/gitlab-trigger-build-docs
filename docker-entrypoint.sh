#!/bin/sh

case $1 in
deploy|cleanup|help)
  exec gitlab-trigger-build-docs "$@"
esac;

exec "$@"
