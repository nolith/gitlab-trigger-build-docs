FROM golang:1.8-alpine AS build

WORKDIR /go/src/gitlab.com/nolith/gitlab-trigger-build-docs
COPY . .

RUN go build

FROM alpine:3.6

RUN apk add -U ca-certificates
COPY --from=build /go/src/gitlab.com/nolith/gitlab-trigger-build-docs/gitlab-trigger-build-docs /usr/bin/gitlab-trigger-build-docs
COPY docker-entrypoint.sh /usr/bin/
ENTRYPOINT [ "/usr/bin/docker-entrypoint.sh" ]
